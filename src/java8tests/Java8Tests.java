package java8tests;

import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 *
 * @author Mario Salazar de Torres
 */
public class Java8Tests
{

  //C/C++ Callback style
  private static final Runnable [] m_Tests =
  {
    Java8Tests::TestForeach,
    Java8Tests::TestStream,
    Java8Tests::TestMapReduce,
    Java8Tests::TestCollectors,
    Java8Tests::TestFunctions,
    Java8Tests::TestFunctionalInterfaces,
  };

  public interface NFunction<T>
  {
    T eval(T ... coords);
  }

  /**
   * Static class method which executes "foreach" tests.
   */
  public static void TestForeach()
  {
    List<String> list = Arrays.asList("One", "Two", "Three", "Four", "Five", "And so on...");

    System.out.println("Testing foreach functionality.");

    //
    System.out.println("Java 7 and below way.");
    for (String s : list)
    {
      System.out.println(s);
    }

    //
    System.out.println("Java 8 and over way using lambda expressions.");
    list.forEach((String s) -> System.out.println(s));

    //
    System.out.println("Java 8 and over way using method referencing.");
    list.forEach(System.out::println);

    System.out.println();
  }

  /**
   * Static class method which executes "java.util.stream.Stream" tests.
   */
  public static void TestStream()
  {
    List<Integer> list = Arrays.asList(144, 1, 1, 21, 8, 5, 3, 55, 2, 34, 13, 89, 0);

    System.out.println("Testing java.util.stream.Stream functionality.");

    System.out.println("All match > -1?: " + list.stream().allMatch(x -> x > -1));
    System.out.println("All match > 0?: " + list.stream().allMatch(x -> x > 0));

    System.out.println("Any match < 0?: " + list.stream().anyMatch(x -> x < 0));
    System.out.println("Any match < 1?: " + list.stream().anyMatch(x -> x < 1));

    System.out.println("None match > 144?: " + list.stream().noneMatch(x -> x > 144));
    System.out.println("None match > 89?: " + list.stream().noneMatch(x -> x > 89));

    System.out.println("Distinct elements from " + list + " are "
            + list.stream().distinct().collect(Collectors.toList()));

    System.out.println("List " + list + " has " + list.stream().count() + " elements.");

    System.out.println("Odd elements from this list " + list + " are "
            + list.stream().filter(x -> (x & 1) == 1).collect(Collectors.toList()));

    System.out.println("Even elements from this list " + list + " are "
            + list.stream().filter(x -> (x & 1) == 0).collect(Collectors.toList()));

    System.out.println("Any element from this list " + list + " is " + list.stream().findAny().get());

    System.out.println("First element from this list " + list + " is " + list.stream().findFirst().get());

    System.out.println("Sorted list for this list " + list + " is "
            + list.stream().sorted().collect(Collectors.toList()));

    System.out.println("Reversed sorted list for this list " + list + " is "
            + list.stream().sorted((x, y) -> y.compareTo(x)).collect(Collectors.toList()));

    System.out.println("3 first elements for this list " + list + " are "
            + list.stream().limit(3).collect(Collectors.toList()));

    System.out.println("Elements for this list " + list
            + " removing 3 first elements are "
            + list.stream().skip(3).collect(Collectors.toList()));

    System.out.println("Limits for this list " + list + " are ["
            + list.stream().min((x, y) -> x.compareTo(y)).get() + ", "
            + list.stream().max((x, y) -> x.compareTo(y)).get() + "]");

    System.out.print("Square of the elements in this list " + list + " are [");
    list.stream().map(x -> x * x).peek(e -> System.out.print(e + ", "))
            .collect(Collectors.toList());
    System.out.println("]");


    System.out.println();
  }

  /**
   * Static class method which executes "java.util.stream.Stream" MapReduce tests.
   */
  public static void TestMapReduce()
  {
    List<String> dictionary = Arrays.asList(
            "must",
            "given",
            "first",
            "a",
            "law",
            "orders",
            "the",
            "it",
            "conflict",
            "robot",
            "human",
            "except",
            "beings",
            "where",
            "orders",
            "such",
            "obey",
            "would",
            "with",
            "by",
            "the"
    );

    List<Integer> list = Arrays.asList(199, 242, 42, 154, 401, 110, 424, 79, 375,
                                       3, 222, 332, 308, 384, 265, 30, 344,
                                       187, 288, 104, 127);

    System.out.println("Testing java.util.stream.Stream MapReduce functionality.");

    System.out.println("Sum for the elements of this list " + list
            + " is " + list.stream().reduce((x, y) -> x + y).get());

    System.out.println("Evaluration of f(x)=x^3 for the elements of this list "
            + list + " correspond to " + list.stream().map(x -> x * x * x)
            .collect(Collectors.toList()));

    System.out.println("Evaluration of Sum(f(x)=sin(8*(x+log (x+1)))/(x+2)) "
            + "for the elements of this list " + list + " is "
            + list.stream().mapToDouble(x -> Math.sin(8 * (x + Math.log(x + 1))) / (x + 2))
            .reduce((x, y) -> x + y).getAsDouble());

    System.out.println("Decodified message given this dictionary " + dictionary
            + " and this key " + list + " is \""
            + list.stream().sorted().map(x -> dictionary.get(x % dictionary.size()))
            .collect(Collectors.joining(" ")) + "\"");


    System.out.println();
  }

  /**
   * Static class method which executes Collectors tests.
   */
  public static void TestCollectors()
  {
    List<Integer> list = Arrays.asList(144, 1, 21, 8, 5, 3, 55, 2, 34, 13, 89, 0);

    System.out.println("Testing collectors functionality.");

    System.out.println("Square of the elements in this list " + list + " are "
            + list.stream().collect(Collectors.toMap(Function.identity(), x -> x * x)));

    System.out.println("Average of the elements in this list " + list + " is "
            + list.stream().collect(Collectors.averagingDouble(x -> x)));

    System.out.println("Grouping of even and odd elements in this list " + list + " is "
            + list.stream().collect(Collectors.groupingBy(x -> x & 1)));

    System.out.println("Join of the element in this list " + list + " is "
            + list.stream().map(x -> x.toString()).collect(Collectors.joining(", ", "[", "]")));

    System.out.println("Statistics summary of the element in this list " + list
            + " is " + list.stream().collect(Collectors.summarizingDouble(x -> x)));

    System.out.println("Set of this list " + list
            + " is " + list.stream().collect(Collectors.toSet()));


    System.out.println();
  }

  /**
   * Static class method which executes "java.util.function" tests.
   */
  public static void TestFunctions()
  {
    List<Integer> list = Arrays.asList(144, 1, 21, 8, 5, 3, 55, 2, 34, 13, 89, 0);

    System.out.println("Testing \"java.util.function\" functionality.");

    System.out.println("First element of this list " + list + " is "
            + EvalPredicateString(list.stream().findFirst().get(), x -> (x & 1) == 0, "even", "odd"));

    try
    {
      System.out.println("Results of the binary operations are " +
              Stream.of("10 + 2", "30 - 3", "40 * 6", "60/50", "80 % 21", "2^32")
              .collect(Collectors.toMap(Function.identity(),
                              x -> ArithmeticBinaryOperate(x))));
    }
    catch (IllegalArgumentException e)
    {
      System.out.println("Invalid operation");
    }

    try
    {
      System.out.println("Results of the unary operations are " +
              Stream.of("- 5", "!20")
              .collect(Collectors.toMap(Function.identity(),
                              x -> ArithmeticUnaryOperate(x))));
    }
    catch (IllegalArgumentException e)
    {
      System.out.println("Invalid operation");
    }

    System.out.println("PI value is " + PiSupplier().get());

    EConsumer().accept(10.);


    System.out.println();
  }

  /**
   * Static class method which executes FunctionalInterface tests.
   */
  public static void TestFunctionalInterfaces()
  {
    List<Double[]> input = Arrays.asList(
            new Double[]
            {
              144., 1., 21., 8.
            }, new Double[]
            {
              5., 3., 55., 2.
            },
            new Double[]
            {
              34., 13., 89., 0.
            }
    );

    System.out.println("Testing FunctionalInterface functionality.");

    NFunction<Double> plane = PlaneFunction(3., 2., -7., 1.);

    input.stream().forEach(x -> System.out.println("Point " + Arrays.asList(x)
            + " evaluation against the plane is " + plane.eval(x)));


    System.out.println();
  }

  /**
   * Supplier for PI
   * @return
   */
  protected static Supplier<Double> PiSupplier()
  {
    return () -> Math.PI;
  }

  /**
   * Consumer for e^x
   * @return
   */
  protected static Consumer<Double> EConsumer()
  {
    return x -> System.out.println("e^" + x + " = " + Math.pow(Math.E, x));
  }

  /**
   * Perform unary operations
   * @param operation
   * @return Result of the operation.
   */
  protected static Double ArithmeticUnaryOperate( String operation )
          throws IllegalArgumentException
  {
    Map<String, UnaryOperator<Double>> operators = new HashMap<>();


    operators.put("-", x -> -x);
    operators.put("!", x -> (double)LongStream.rangeClosed(2, Math.round(x))
            .reduce(1, (y, z) -> y * z));

    Matcher m = Pattern.compile("([\\!\\-])\\s*(.+)")
            .matcher(operation);

    if (!m.matches())
    {
      throw new IllegalArgumentException();
    }

    UnaryOperator<Double> op = operators.get(m.group(1));
    if (op == null)
    {
      throw new IllegalArgumentException();
    }

    try
    {
      return op.apply(Double.parseDouble(m.group(2)));
    }
    catch (NumberFormatException ex)
    {
      throw new IllegalArgumentException();
    }
  }

  /**
   * Perform binary operations
   * @param operation
   * @return Result of the operation.
   */
  protected static Double ArithmeticBinaryOperate( String operation )
          throws IllegalArgumentException
  {
    Map<String, BinaryOperator<Double>> bioperators = new HashMap<>();

    bioperators.put("+", (x, y) -> x + y);
    bioperators.put("-", (x, y) -> x - y);
    bioperators.put("*", (x, y) -> x * y);
    bioperators.put("/", (x, y) -> x / y);
    bioperators.put("%", (x, y) -> x % y);
    bioperators.put("^", (x, y) -> Math.pow(x, y));

    Matcher m = Pattern.compile("(.+)\\s*([\\+\\-\\*/%\\^])\\s*(.+)")
            .matcher(operation);

    if (!m.matches())
    {
      throw new IllegalArgumentException();
    }

    BinaryOperator<Double> op = bioperators.get(m.group(2));
    if (op == null)
    {
      throw new IllegalArgumentException();
    }

    try
    {
      return op.apply(Double.parseDouble(m.group(1)),
              Double.parseDouble(m.group(3)));
    }
    catch (NumberFormatException ex)
    {
      throw new IllegalArgumentException();
    }
  }

  protected static <T> String EvalPredicateString(T item, Predicate<T> pred, String t, String f)
  {
    return pred.test(item) ? t : f;
  }

  protected static NFunction<Double> PlaneFunction(Double a, Double b, Double c, Double d)
  {
    return x -> a*x[0] + b*x[1] + c*x[2] + d;
  }

  /**
   * @param args the command line arguments
   */
  public static void main( String[] args )
  {
    System.out.println("Running tests: ");
    Arrays.asList(m_Tests).forEach(Runnable::run);
  }

}
